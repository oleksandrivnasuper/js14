// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки

const allBtn = document.querySelectorAll('.button');
const boarderSecond = document.querySelector('.table_container2');
const theadBoarderSecond = document.querySelector('.thead_second');
const boarderFirst = document.querySelector('.table_container1');

const changeBtn = document.getElementById('change-style');
let btnKey = 'change';
let userClick = localStorage.getItem(btnKey);

let starter = true;

if (userClick) {
newStyle();

}else{
    originalStyle();
}


function newStyle(){
    localStorage.setItem(btnKey, true);
    changeBtn.style.backgroundColor = '#550736';
    changeBtn.style.color = '#fff';
    boarderFirst.style.borderColor = '#b11673';
    boarderSecond.style.borderColor = '#b11673';
    theadBoarderSecond.style.borderColor = '#b11673';
   
    allBtn.forEach((el) => {
        el.style.backgroundColor = '#550736';
        el.style.borderBottom = '15px solid #bf75a1';
    })
}

function originalStyle(){
   
    changeBtn.style.removeProperty('background-color');
    changeBtn.style.removeProperty('color');
    boarderFirst.style.removeProperty('border-color');
    boarderSecond.style.removeProperty('border-color');
    theadBoarderSecond.style.removeProperty('border-color');
   
    allBtn.forEach((el) => {
        el.style.removeProperty('background-color');
        el.style.removeProperty('border-bottom');
    })
}


function Change(){

    if(starter === true){
       newStyle();
        starter = false;
       
    }else{
        originalStyle();
        starter = true;
    }
}

changeBtn.addEventListener('click', Change);